import React, { useMemo, useState } from "react";
import ChildUseMemo from "./ChildUseMemo";

export default function DemoHookUseMemo() {
  const [like, setLike] = useState(1);
  let cart = [
    { id: 1, name: "iPhone", price: 1000 },
    { id: 2, name: "HTC Phone", price: 2000 },
    { id: 3, name: "LG Phone", price: 3000 },
  ];
  const cartMemo = useMemo(() => cart, []);
  return (
    <div className="m-5">
      Like: {like} ♥️
      <br />
      <span
        style={{ cursor: "pointer", color: "red", fontSize: 35 }}
        onClick={() => setLike(like + 1)}
      >
        ♥️
      </span>
      <br />
      <br />
      <ChildUseMemo cart={cartMemo} />
    </div>
  );
}
