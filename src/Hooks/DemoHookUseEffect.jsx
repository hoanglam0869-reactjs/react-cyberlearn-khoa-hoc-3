import React, { useEffect, useState } from "react";
import ChildUseEffect from "./ChildUseEffect";

export default function DemoHookUseEffect() {
  const [number, setNumber] = useState(1);

  const [like, setLike] = useState(1);
  console.log(like);
  // useEffect là hàm chạy sau khi giao diện render thay cho didUpdate và didMount trong mọi trường hợp
  /* useEffect(() => {
    console.log("did mount");
    console.log("did update");
  }); */

  // Cách viết thay thế cho component did mount
  useEffect(() => {
    console.log("did mount");
  }, []);

  // Cách viết thay thế cho component did update
  useEffect(() => {
    console.log("did update khi number thay đổi");
    // number là giá trị nếu bị thay đổi sau render thì useEffect này sẽ chạy
    return () => {
      console.log("will unmount");
    };
  }, [number]);

  console.log("render");

  const handleIncrease = () => {
    let newNumber = number + 1;
    setNumber(newNumber);
  };

  const handleLike = () => setLike(like + 1);

  return (
    <div className="m-5">
      <button onClick={handleLike}>Like</button>
      <div className="card text-start" style={{ width: 200, height: 300 }}>
        <img
          style={{ width: 200, height: 200 }}
          className="card-img-top"
          src="https://picsum.photos/200/200"
          alt="123"
        />
        <div className="card-body">
          <h4 className="card-title text-danger">{number} ♥️</h4>
        </div>
      </div>
      <button className="btn btn-success" onClick={handleIncrease}>
        +
      </button>
      {like == 1 ? <ChildUseEffect /> : ""}
    </div>
  );
}
