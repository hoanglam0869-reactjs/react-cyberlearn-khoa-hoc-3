import React, { useEffect, useState } from "react";

export default function ChildUseEffect() {
  const [number, setNumber] = useState(1);

  console.log("render ChildUseEffect");

  useEffect(() => {
    // Viết cho did update
    console.log("did update");
    return () => {
      console.log("will unmount");
    };
  }, [number]);

  return (
    <div>
      <textarea></textarea>
      <br />
      <br />
      <button className="btn btn-success">Gửi</button>
    </div>
  );
}
