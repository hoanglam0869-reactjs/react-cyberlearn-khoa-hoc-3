import React, { useRef, useState } from "react";

export default function DemoUseRef() {
  const inputUserName = useRef(null);
  const inputPassword = useRef(null);

  let userName = useRef("");

  const [userLogin, setUserLogin] = useState({ userName: "" });

  const handleLogin = () => {
    // console.log("inputUserName", inputUserName.current.name);
    // console.log("inputPassword", inputPassword.current.value);

    console.log("userName", userName.current);
    console.log("userLogin", userLogin.userName);

    userName.current = "abc";
    setUserLogin({
      userName: userName.current,
    });
  };

  return (
    <div className="container">
      <h3>Login</h3>
      <div className="form-group">
        <h3>Username</h3>
        <input
          type="text"
          ref={inputUserName}
          name="userName"
          className="form-control"
        />
      </div>
      <div className="form-group">
        <h3>Password</h3>
        <input
          type="text"
          ref={inputPassword}
          name="passWord"
          className="form-control"
        />
      </div>
      <div className="form-group">
        <button className="btn btn-success" onClick={handleLogin}>
          Login
        </button>
      </div>
    </div>
  );
}
