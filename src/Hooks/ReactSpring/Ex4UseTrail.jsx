import React, { useState } from "react";
import { animated, useTrail } from "react-spring";

const items = [
  { title: "FrontEndOnline", content: "cyberlearn" },
  { title: "FrontEndOffline", content: "cybersoft" },
  { title: "FrontEndTuXa", content: "cybersoft" },
];

export default function Ex4UseTrail() {
  const [status, setStatus] = useState(true);

  /* const [propsUseTrail, set, stop] = useTrail(items.length, () => ({
    opacity: status ? 1 : 0,
    x: status ? 0 : 20,
    height: status ? 80 : 0,
    color: "red",
    from: { opacity: 0, x: 20, height: 0, color: "green" },
    config: { duration: 2000 },
  })); */

  const propsUseTrail = useTrail(items.length, {
    opacity: status ? 1 : 0,
    x: status ? 0 : 20,
    height: status ? 80 : 0,
    color: "red",
    from: { opacity: 0, x: 20, height: 0, color: "green" },
    config: { duration: 2000 },
  });

  /* set({
    opacity: status ? 1 : 0,
    x: status ? 0 : 20,
    height: status ? 80 : 0,
    color: "red",
    from: { opacity: 0, x: 20, height: 0, color: "green" },
    config: { duration: 2000 },
  }); */
  return (
    <div>
      <button onClick={() => setStatus(!status)}>Set Status</button>
      {/* <button onClick={() => set({ color: "black" })}>Set Color</button> */}
      {propsUseTrail.map((propsUseSprings, index) => (
        <animated.h1 key={index} style={propsUseSprings}>
          {items[index].title}
        </animated.h1>
      ))}
    </div>
  );
}
