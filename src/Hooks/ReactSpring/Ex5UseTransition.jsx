import React, { useState } from "react";
import { animated, useTransition } from "react-spring";

/* export default function Ex5UseTransition() {
  const [arrItem, setArrItem] = useState([
    { id: 1, title: "FrontEndOnline", content: "cyberlearn" },
    { id: 2, title: "FrontEndOffline", content: "cybersoft" },
    { id: 3, title: "FrontEndTuXa", content: "cybersoft" },
  ]);

  const propsUseTransition = useTransition(arrItem, item => item.id, {
    from: { transform: "translate3d(0, -40px, 0)" }, // Component từ vị trí trước khi render
    enter: { transform: "translate3d(0, 0px, 0)" }, // Component tại thời điểm render
    leave: { transform: "translate3d(0, -40px, 0)" }, // Component bị xóa mất khỏi giao diện
    config: { duration: 2000 },
  });

  let renderItem = () => {
    return propsUseTransition.map(({ props, item, key }, index) => (
      <animated.div
        style={props}
        key={index}
        className="bg-dark text-white p-3 mt-2"
      >
        <h1>{item.title}</h1>
        <p>{item.content}</p>
      </animated.div>
    ));
  };

  return <div className="container">{renderItem()}</div>;
} */

export default function Ex5UseTransition() {
  const [arrItem, setArrItem] = useState([
    { id: 1, title: "FrontEndOnline", content: "cyberlearn" },
    { id: 2, title: "FrontEndOffline", content: "cybersoft" },
    { id: 3, title: "FrontEndTuXa", content: "cybersoft" },
  ]);

  const [article, setArticle] = useState({
    id: "",
    title: "",
    content: "",
  });

  /* const [propsUseTransition, api] = useTransition(arrItem, () => ({
      from: { transform: "translate3d(0, -40px, 0)" }, // Component từ vị trí trước khi render
      enter: { transform: "translate3d(0, 0px, 0)" }, // Component tại thời điểm render
      leave: { transform: "translate3d(0, -40px, 0)" }, // Component bị xóa mất khỏi giao diện
      config: { duration: 2000 },
    })); */

  const propsUseTransition = useTransition(arrItem, {
    from: { transform: "translate3d(0, -40px, 0)" }, // Component từ vị trí trước khi render
    enter: { transform: "translate3d(0, 0px, 0)" }, // Component tại thời điểm render
    leave: { transform: "translate3d(0, -40px, 0)" }, // Component bị xóa mất khỏi giao diện
    config: { duration: 2000 },
  });

  let renderItem = () => {
    return propsUseTransition((style, item) => (
      <animated.div style={style} className="bg-dark text-white p-3 mt-2">
        <div className="text-end">
          <button
            className="btn btn-danger"
            onClick={() => deleteItem(item.id)}
          >
            X
          </button>
        </div>
        <h1>{item.title}</h1>
        <p>{item.content}</p>
      </animated.div>
    ));
  };

  let handleChange = (e) => {
    let { value, name } = e.target;
    setArticle({
      ...article,
      [name]: value,
    });
  };

  let handleSubmit = () => {
    let item = { ...article, id: Date.now() };
    setArrItem([...arrItem, item]);
  };

  let deleteItem = (id) => {
    // set lại arrItem mới bằng cách lấy những phần tử khác id đó => giống như tìm index xóa và cập nhật lại
    setArrItem(arrItem.filter((item) => item.id != id));
  };

  return (
    <div className="container">
      {renderItem()}
      <div className="form-group">
        <h3>Title</h3>
        <input
          type="text"
          className="form-control"
          name="title"
          onChange={handleChange}
        />
      </div>
      <div className="form-group">
        <h3>Content</h3>
        <input
          type="text"
          className="form-control"
          name="content"
          onChange={handleChange}
        />
      </div>
      <div className="form-group">
        <button className="btn btn-success mt-2" onClick={handleSubmit}>
          Add article
        </button>
      </div>
    </div>
  );
}
