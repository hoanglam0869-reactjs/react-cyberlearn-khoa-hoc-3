import React, { useState } from "react";
import {
  animated,
  useChain,
  useSpring,
  useSpringRef,
  useTransition,
} from "react-spring";

export default function Ex6UseChain() {
  const [arrContent, setArrContent] = useState([
    { id: 1, title: "FrontEndOnline", content: "cyberlearn" },
    { id: 2, title: "FrontEndOffline", content: "cybersoft" },
    { id: 3, title: "FrontEndTuXa", content: "cybersoft" },
  ]);

  // Tạo 1 useSpring animation
  let springRef = useSpringRef();
  const propsAnim = useSpring({
    opacity: 1,
    width: "100%",
    height: "100%",
    from: { opacity: 0, width: "0%", height: "0%" },
    config: { duration: 500 },
    ref: springRef,
  });

  // Tạo 1 useTransition
  let transitionRef = useSpringRef();
  const transitions = useTransition(arrContent, {
    from: { transform: "translate3d(0, -40px, 0)" },
    enter: {
      transform: "translate3d(0, 0px, 0)",
      opacity: 1,
      width: "100%",
      height: "100%",
    },
    leave: {
      transform: "translate3d(0, -40px, 0)",
      opacity: 0,
      width: "100%",
      height: "0%",
    },
    config: { duration: 500 },
    ref: transitionRef,
  });

  // Kết hợp 2 useSpring
  // timesteps: [0, 1], timeframe: 1000
  // for the first ref this would be 0 because 0 * 1000 = 0
  // for the second ref this would be 1000 because 1 * 1000 = 1000
  useChain([springRef, transitionRef], [0, 0.5], 1000);

  let deleteItem = (id) => {
    setArrContent([...arrContent.filter((article) => article.id != id)]);
  };

  return (
    // Sử dụng lồng 2 hook cùng lúc
    <div className="container">
      <animated.div style={propsAnim}>
        {transitions((style, item) => (
          <animated.div
            key={item.id}
            style={style}
            className="my-2 bg-dark text-white p-3"
          >
            <div className="text-end">
              <button
                className="btn btn-danger"
                onClick={() => deleteItem(item.id)}
              >
                X
              </button>
            </div>
            <h3>{item.title}</h3>
            <p>{item.content}</p>
          </animated.div>
        ))}
      </animated.div>
    </div>
  );
}
