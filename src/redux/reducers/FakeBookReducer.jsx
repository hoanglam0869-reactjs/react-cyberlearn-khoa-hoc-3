import { ADD_COMMENT } from "../types/FaceBookType";

const stateDefault = {
  comments: [
    {
      name: "Yone",
      content: "Hi! Yasuo",
      avatar: `https://i.pravatar.cc/50?u=yone`,
    },
    {
      name: "Yasuo",
      content: "Hi! brother",
      avatar: `https://i.pravatar.cc/50?u=yasuo`,
    },
  ],
};

const FakebookReducer = (state = stateDefault, action) => {
  switch (action.type) {
    case ADD_COMMENT: {
      state.comments = [...state.comments, action.userComment];
      return { ...state };
    }
  }
  return { ...state };
};

export default FakebookReducer;
