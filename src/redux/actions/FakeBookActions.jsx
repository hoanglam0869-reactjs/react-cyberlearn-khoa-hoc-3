import { ADD_COMMENT } from "../types/FaceBookType";

export const addCommentAction = (userComment) => ({
  type: ADD_COMMENT,
  userComment,
});
