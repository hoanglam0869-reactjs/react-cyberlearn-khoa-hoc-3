const toDoListState = {
  toDo: "",
  toDoList: ["Học FE", "Học BE"],
};

export const ToDoListReducer = (state = toDoListState, action) => {
  switch (action.type) {
    case "HANDLE_CHANGE": {
      return { ...state, toDo: action.value };
    }
    case "ADD_TASK": {
      let toDoList = [...state.toDoList];
      toDoList.push(action.task);
      return { ...state, toDoList: toDoList };
    }
  }
  return { ...state };
};
