import { ToDoListDarkTheme } from "../JSS_StyledComponent/Themes/ToDoListDarkTheme";
import { ToDoListLightTheme } from "../JSS_StyledComponent/Themes/ToDoListLightTheme";
import { ToDoListPrimaryTheme } from "../JSS_StyledComponent/Themes/ToDoListPrimaryTheme";

const themeState = {
  theme: ToDoListDarkTheme,
};

export const ThemeReducer = (state = themeState, action) => {
  switch (action.type) {
    case "CHANGE_THEME": {
      switch (action.theme) {
        case "0":
          state.theme = ToDoListPrimaryTheme;
          break;
        case "1":
          state.theme = ToDoListDarkTheme;
          break;
        case "2":
          state.theme = ToDoListLightTheme;
          break;
      }
      return { ...state };
    }
  }
  return { ...state };
};
