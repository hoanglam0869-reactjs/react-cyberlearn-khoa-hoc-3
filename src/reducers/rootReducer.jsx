import { combineReducers } from "redux";
import { ThemeReducer } from "./ThemeReducer";
import { ToDoListReducer } from "./ToDoListReducer";

export const rootReducer = combineReducers({
  ThemeReducer: ThemeReducer,
  ToDoListReducer: ToDoListReducer,
});
