import React, { Component } from "react";
import { ThemeProvider } from "styled-components";
import { Container } from "../../ComponentsToDoList/Container";
import { Dropdown } from "../../ComponentsToDoList/Dropdown";
import { connect } from "react-redux";
import { Heading3 } from "../../ComponentsToDoList/Heading";
import { TextField } from "../../ComponentsToDoList/TextField";
import { Button } from "../../ComponentsToDoList/Button";
import {
  Table,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
} from "../../ComponentsToDoList/Table";
import {
  addTaskAction,
  changeThemeAction,
  deleteTaskAction,
  doneTaskAction,
  editTaskAction,
  updateTaskAction,
} from "../../../redux/actions/ToDoListActions";
import { arrTheme } from "../../Themes/ThemeManager";

class ToDoList extends Component {
  state = {
    taskName: "",
  };

  renderTaskToDo = () => {
    return this.props.taskList.map((task, index) =>
      task.done ? null : (
        <Tr key={index}>
          <Td style={{ verticalAlign: "middle" }}>{task.taskName}</Td>
          <Td className="text-end">
            <Button
              disabled={this.props.isUpdating}
              onClick={() => this.editTask(task)}
            >
              <i className="fa fa-edit"></i>
            </Button>
            <Button
              className="mx-1"
              disabled={this.props.isUpdating}
              onClick={() => this.doneTask(task.id)}
            >
              <i className="fa fa-check"></i>
            </Button>
            <Button disabled={this.props.isUpdating}>
              <i
                className="fa fa-trash"
                onClick={() => this.deleteTask(task.id)}
              ></i>
            </Button>
          </Td>
        </Tr>
      )
    );
  };

  renderTaskCompleted = () => {
    return this.props.taskList.map((task, index) =>
      task.done ? (
        <Tr key={index}>
          <Td style={{ verticalAlign: "middle" }}>{task.taskName}</Td>
          <Td className="text-end">
            <Button disabled={this.props.isUpdating}>
              <i
                className="fa fa-trash"
                onClick={() => this.deleteTask(task.id)}
              ></i>
            </Button>
          </Td>
        </Tr>
      ) : null
    );
  };

  handleChange = (e) => {
    this.setState({
      taskName: e.target.value,
    });
  };

  addTask = () => {
    // Lấy thông tin người dùng nhập vào từ input
    let { taskName } = this.state;
    // Tạo ra 1 task object
    let newTask = {
      id: Date.now(),
      taskName: taskName,
      done: false,
    };
    // Đưa task object lên redux thông qua phương thức dispatch
    this.props.dispatch(addTaskAction(newTask));
  };
  // Viết hàm render theme import ThemeManager
  renderTheme = () => {
    return arrTheme.map((theme, index) => (
      <option key={index} value={theme.id}>
        {theme.name}
      </option>
    ));
  };

  changeTheme = (e) => {
    let { value } = e.target;
    // Dispatch value lên reducer
    this.props.dispatch(changeThemeAction(value));
  };

  doneTask = (taskId) => this.props.dispatch(doneTaskAction(taskId));

  deleteTask = (taskId) => this.props.dispatch(deleteTaskAction(taskId));

  editTask = (task) => this.props.dispatch(editTaskAction(task));

  updateTask = () => {
    let { taskName } = this.state;
    /* this.setState(
      {
        taskName: "",
      },
      () => this.props.dispatch(updateTaskAction(taskName))
    ); */
    this.props.dispatch(updateTaskAction(taskName));
  };

  // Lifecycle bằng 16 nhận vào props mới được thực thi trước render
  /* componentWillReceiveProps(newProps) {
    console.log("this.props", this.props);
    console.log("newProps", newProps);
    this.setState({
      taskName: newProps.taskEdit.taskName,
    });
  } */

  // Lifecycle tĩnh không truy xuất được trỏ this
  /* static getDerivedStateFromProps(newProps, currentState) {
    // newProps là props mới, props cũ là this.props (không truy xuất được)
    // currentState: ứng với state hiện tại this.state

    // hoặc trả về state mới (this.state)
    let newState = { ...currentState, taskName: newProps.taskEdit.taskName };
    return newState;

    // trả về null state giữ nguyên
    // return null;
  } */

  render() {
    return (
      <ThemeProvider theme={this.props.themeToDoList}>
        <Container className="w-50">
          <Dropdown onChange={this.changeTheme}>{this.renderTheme()}</Dropdown>
          <Heading3>To do list</Heading3>
          <TextField
            name="taskName"
            className="w-50"
            label="Task name"
            value={this.state.taskName}
            onChange={this.handleChange}
          />
          <Button
            className="ms-2"
            disabled={this.props.isUpdating}
            onClick={this.addTask}
          >
            <i className="fa fa-plus"></i> Add task
          </Button>
          {this.props.isUpdating ? (
            <Button
              className="ms-2"
              disabled={!this.props.isUpdating}
              onClick={this.updateTask}
            >
              <i className="fa fa-upload"></i> Update task
            </Button>
          ) : (
            ""
          )}
          <Heading3>Task to do</Heading3>
          <Table>
            <Thead>
              <Tr>
                <Th>Task name</Th>
                <Th className="text-end"></Th>
              </Tr>
            </Thead>
            <Tbody>{this.renderTaskToDo()}</Tbody>
          </Table>
          <Heading3>Task completed</Heading3>
          <Table>
            <Thead>
              <Tr>
                <Th>Task name</Th>
                <Th className="text-end"></Th>
              </Tr>
            </Thead>
            <Tbody>{this.renderTaskCompleted()}</Tbody>
          </Table>
        </Container>
      </ThemeProvider>
    );
  }

  // Đây là lifecycle trả về props cũ và state cũ của component trước khi render (lifecycle này chạy sau render)
  componentDidUpdate(prevProps, prevState) {
    // So sánh nếu như props trước đó (taskEdit trước mà khác taskEdit hiện tại thì mình mới setState)
    if (prevProps.taskEdit.id != this.props.taskEdit.id) {
      this.setState({
        taskName: this.props.taskEdit.taskName,
      });
    }
  }
}

const mapStateToProps = (state) => {
  return {
    themeToDoList: state.ToDoListReducer.themeToDoList,
    taskList: state.ToDoListReducer.taskList,
    isUpdating: state.ToDoListReducer.isUpdating,
    taskEdit: state.ToDoListReducer.taskEdit,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    changeTheme: (event) => {
      const action = {
        type: "CHANGE_THEME",
        theme: event.target.value,
      };
      dispatch(action);
    },
    handleChange: (event) => {
      const action = {
        type: "HANDLE_CHANGE",
        value: event.target.value,
      };
      dispatch(action);
    },
    addTask: (task) => {
      if (task.trim() == "") return;
      const action = {
        type: "ADD_TASK",
        task: task,
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps)(ToDoList);
