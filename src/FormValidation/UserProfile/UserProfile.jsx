import React, { Component } from "react";
import "./UserProfile.css";
import Swal from "sweetalert2";

export default class UserProfile extends Component {
  state = {
    values: {
      firstName: "",
      lastName: "",
      userName: "",
      email: "",
      passWord: "",
      passWordConfirm: "",
    },
    errors: {
      firstName: "",
      lastName: "",
      userName: "",
      email: "",
      passWord: "",
      passWordConfirm: "",
    },
  };

  handleChangeValue = (event) => {
    let { name, value, type, pattern } = event.target;

    let newValues = { ...this.state.values, [name]: value };
    let newErrors = { ...this.state.errors };

    if (value.trim() == "") {
      newErrors[name] = name + " is required!";
    } else {
      newErrors[name] = "";
    }

    if (type == "email") {
      const regexEmail = new RegExp(pattern, "g");
      // Nếu email không hợp lệ
      if (!regexEmail.test(value)) {
        newErrors[name] = name + " is invalid!";
      } else {
        // Nếu email hợp lệ
        newErrors[name] = "";
      }
    }

    if (name == "passWordConfirm") {
      if (value == newValues["passWord"]) {
        newErrors[name] = "";
      } else {
        newErrors[name] = name + " is invalid";
      }
    }

    this.setState({
      values: newValues,
      errors: newErrors,
    });
  };

  handleSubmit = (event) => {
    // Cản trình duyệt submit reload lại trang
    event.preventDefault();
    // Xét điều kiện khi submit
    let { values, errors } = this.state;
    // Biến xác định form hợp lệ
    let valid = true;
    let profileContent = "";
    let errorsContent = "";

    for (let key in values) {
      if (values[key] == "") {
        errorsContent += `<p class="text-left"><b class="text-danger">${key} is invalid!</b></p>`;
        valid = false;
      }

      profileContent += `<p class="text-left"><b>${key}:</b> ${values[key]}</p>`;
    }

    for (let key in errors) {
      if (errors[key] != "") {
        errorsContent += `<p class="text-left"><b class="text-danger">${key} is invalid!</b></p>`;
        valid = false;
      }
    }

    if (!valid) {
      Swal.fire({
        title: "Your profile",
        html: errorsContent,
        icon: "error", // success, error, warning, question
        confirmButtonText: "OK",
      });
      alert("Dữ liệu chưa hợp lệ!");
      return;
    }

    // alert('Thành công!')
    Swal.fire({
      title: "Your profile",
      html: profileContent,
      icon: "success", // success, error, warning, question
      confirmButtonText: "OK",
    });
  };

  render() {
    return (
      <div
        className="container-fluid"
        style={{
          backgroundColor: "#EEEEEE",
          display: "flex",
          justifyContent: "center",
        }}
      >
        <form
          onSubmit={this.handleSubmit}
          style={{ fontFamily: "Google Sans", width: 600 }}
          className="bg-white p-5 m-5"
        >
          <h1 className="text-center mt-0 mb-5">User Profile</h1>
          <div className="row">
            <div className="col-6">
              <div className="group">
                <input
                  value={this.state.values.firstName}
                  type="text"
                  name="firstName"
                  required
                  onChange={this.handleChangeValue}
                />
                <span className="highlight" />
                <span className="bar" />
                <label>firstName</label>
                <span className="text text-danger">
                  {this.state.errors.firstName}
                </span>
              </div>
            </div>
            <div className="col-6">
              <div className="group">
                <input
                  value={this.state.values.lastName}
                  type="text"
                  name="lastName"
                  required
                  onChange={this.handleChangeValue}
                />
                <span className="highlight" />
                <span className="bar" />
                <label>lastName</label>
                <span className="text text-danger">
                  {this.state.errors.lastName}
                </span>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <div className="group">
                <input
                  value={this.state.values.userName}
                  type="text"
                  name="userName"
                  required
                  onChange={this.handleChangeValue}
                />
                <span className="highlight" />
                <span className="bar" />
                <label>userName</label>
                <span className="text text-danger">
                  {this.state.errors.userName}
                </span>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <div className="group">
                <input
                  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
                  value={this.state.values.email}
                  type="email"
                  name="email"
                  required
                  onChange={this.handleChangeValue}
                />
                <span className="highlight" />
                <span className="bar" />
                <label>email</label>
                <span className="text text-danger">
                  {this.state.errors.email}
                </span>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-6">
              <div className="group">
                <input
                  value={this.state.values.passWord}
                  type="password"
                  name="passWord"
                  required
                  onChange={this.handleChangeValue}
                />
                <span className="highlight" />
                <span className="bar" />
                <label>passWord</label>
                <span className="text text-danger">
                  {this.state.errors.passWord}
                </span>
              </div>
            </div>
            <div className="col-6">
              <div className="group">
                <input
                  value={this.state.values.passWordConfirm}
                  type="password"
                  name="passWordConfirm"
                  required
                  onChange={this.handleChangeValue}
                />
                <span className="highlight" />
                <span className="bar" />
                <label>passWordConfirm</label>
                <span className="text text-danger">
                  {this.state.errors.passWordConfirm}
                </span>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <button
                className="btn text-white bg-dark w-100"
                style={{ fontSize: 25 }}
              >
                Submit
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}
