import logo from "./logo.svg";
import "./App.css";
import UserProfile from "./FormValidation/UserProfile/UserProfile";
import DemoJSS from "./JSS_StyledComponent/DemoJSS/DemoJSS";
import DemoTheme from "./JSS_StyledComponent/Themes/DemoTheme";
import ToDoList from "./JSS_StyledComponent/BaiTapStyledComponent/ToDoList/ToDoList";
import LifeCycleReact from "./LifeCycleReact/LifeCycleReact";
import DemoHookUseState from "./Hooks/DemoHookUseState";
import DemoHookUseEffect from "./Hooks/DemoHookUseEffect";
import DemoHookUseCallBack from "./Hooks/DemoHookUseCallBack";
import DemoHookUseMemo from "./Hooks/DemoHookUseMemo";
import DemoUseRef from "./Hooks/DemoUseRef";
import DemoUseReducer from "./Hooks/DemoUseReducer";
import DemoUseContext from "./Hooks/DemoUseContext";
import ContextProvider from "./Hooks/Context/ContextProvider";
import DemoReduxApp from "./Hooks/DemoReduxApp";
import DemoUseSpring from "./Hooks/ReactSpring/DemoUseSpring";
import Ex2UseSpring from "./Hooks/ReactSpring/Ex2UseSpring";
import Ex3UseSprings from "./Hooks/ReactSpring/Ex3UseSprings";
import Ex4UseTrail from "./Hooks/ReactSpring/Ex4UseTrail";
import Ex5UseTransition from "./Hooks/ReactSpring/Ex5UseTransition";
import Ex6UseChain from "./Hooks/ReactSpring/Ex6UseChain";

function App() {
  return (
    <ContextProvider>
      {/* <UserProfile /> */}
      {/* <DemoJSS /> */}
      {/* <DemoTheme /> */}
      {/* <ToDoList /> */}
      {/* <LifeCycleReact /> */}
      {/* <DemoHookUseState /> */}
      {/* <DemoHookUseEffect /> */}
      {/* <DemoHookUseCallBack /> */}
      {/* <DemoHookUseMemo /> */}
      {/* <DemoUseRef /> */}
      {/* <DemoUseReducer /> */}
      {/* <DemoUseContext /> */}
      {/* <DemoReduxApp /> */}
      {/* <DemoUseSpring /> */}
      {/* <Ex2UseSpring /> */}
      {/* <Ex3UseSprings /> */}
      {/* <Ex4UseTrail /> */}
      {/* <Ex5UseTransition /> */}
      <Ex6UseChain />
    </ContextProvider>
  );
}

export default App;
